public class Event
{
  int time;
  String name;

  public Event(int t, String n)
  {
    time = t;
    name = n;
  }

  public String getValue()
  {
    return name;
  }

  public int getKey()
  {
    return time;
  }

  public void setValue(String n)
  {
    name = n;
  }

  public void setKey(Integer t)
  {
    time = t;
  }
}
