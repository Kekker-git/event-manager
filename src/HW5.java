/*

  Author: Calvin Williams
  Email: cwilliams2016@my.fit.edu
  Course: CSE 2010
  Section: 3
  Description:

*/

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class HW5
{

  // A program to utilize a Skip List implementation

  /** The skip list for all the events */
  public static SkipListMap eventMap = new SkipListMap();

  /** Adds an event to the skip list */
  public static void AddEvent(int time, String event)
  {
    Event e = eventMap.put(time, event);
    if(e.getValue() != event)
      System.out.println("AddEvent " + time + " ExistingEventError:" +
                         e.getValue());
    else
      System.out.println("AddEvent " + e.getKey() + " " + e.getValue());
  }

  /** Removes the tower containing the event at the given time */
  public static void CancelEvent(int time)
  {
    Event canceled = eventMap.remove(time);
    if(canceled.getKey() != time)
      System.out.println("CancelEvent " + time + " NoEventError");
    else
      System.out.println("CancelEvent " + canceled.getKey() + " " +
                         canceled.getValue());
  }

  /** Prints the event at the given time, or none if there isn't one */
  public static void GetEvent(int time)
  {
    Node event = eventMap.get(time);
    int key = event.getElement().getKey();
    if(key == time) // Check to see if the gotten event has the right key
      System.out.println("GetEvent " + time + " " + event.getElement().getValue());
    else
      System.out.println("GetEvent " + time + " none");
  }

  /** Finds all the events between the given start and end time (inclusive) */
  public static void GetEventsBetweenTimes(int startTime, int endTime)
  {
    System.out.print("GetEventsBetweenTimes " + startTime + " " + endTime + " ");
    Node event = eventMap.get(startTime);
    boolean printFlag = false;

    // Go to the bottom, where the full list is present
    while(event.getBelow() != null)
      event = event.getBelow();

    // Navigate to the first event between the times, if
    // event is not already there
    while(event.getElement().getKey() <= startTime)
      event = event.getNext();
    // Print each element until the end event
    while(event.getElement().getKey() <= endTime)
    {
      System.out.print(event.getElement().getKey() + ":" +
                       event.getElement().getValue() + " ");
      printFlag = true;
      event = event.getNext();
    }

    if(!printFlag)
      System.out.println(" none");
    else
      System.out.println();
  }

  /** Find all events on a given day */
  public static void GetEventsForOneDay(int date)
  {
    System.out.print("GetEventsForOneDay " + date);

    /*
     * This is essentially the same as the get(k) method in
     * SkipListMap.java, with some minor differences.
     */
    Node p = eventMap.getHead(); // Start the search at the head,
                                 // which should be at the top
    boolean printFlag = false;

    while(p.getBelow() != null)  // If you can go down...
    {
      p = p.getBelow(); // ...proceed to do so
      while(date >= p.getNext().getElement().getKey() / 100 &&
            p.getElement().getValue() != "oo")
      {
        p = p.getNext();
      }
    }

    // Go to the bottom after finding the approximate location
    // of the date
    while(p.getBelow() != null)
      p = p.getBelow();

    // Check the previous element, in case the "binary" search
    // skipped it.
    while(p.getPrev().getElement().getKey() / 100 == date)
      p = p.getPrev();

    while(p.getElement().getKey() / 100 == date)
    {
      // Check for the key without the hours, instead of
      // just comparing keys.
      if (p.getElement().getKey() / 100 == date)
        System.out.print(" " + p.getElement().getKey() + ":" +
                           p.getElement().getValue());
      printFlag = true;
      p = p.getNext();
    }
    if(!printFlag)
      System.out.println(" none");
    else
      System.out.println();
  }

  /** Find all events after the given time, but only on the same day */
  public static void GetEventsForTheRestOfTheDay(int currentTime)
  {
    System.out.print("GetEventsForTheRestOfTheDay " + currentTime);
    boolean printFlag = false;
    /*
     * Again, this is a lot like the get(k) method. This one
     * is the same as GetEventsForOneDay, just with a different
     * backwards check.
     */
    Node p = eventMap.getHead(); // Start the search at the head,
                                 // which should be at the top

    while(p.getBelow() != null)  // If you can go down...
    {
      p = p.getBelow(); // ...proceed to do so
      while(currentTime / 100 >= p.getNext().getElement().getKey() / 100 &&
            p.getElement().getValue() != "oo")
      {
        p = p.getNext();
      }
    }

    // Go to the bottom after finding the approximate location
    // of the currentTime
    while(p.getBelow() != null)
      p = p.getBelow();

    // Backwards check
    while(p.getPrev().getElement().getKey() >= currentTime)
      p = p.getPrev();

    while(p.getElement().getKey() / 100 == currentTime / 100)
    {
      // Check for the key without the hours, instead of
      // just comparing keys.
      if (p.getElement().getKey() / 100 == currentTime / 100)
      {
        System.out.print(" " + p.getElement().getKey() + ":" +
                           p.getElement().getValue());
        printFlag = true;
      }
      p = p.getNext();
    }
    if(!printFlag)
      System.out.println(" none");
    else
      System.out.println();
  }

  public static void GetEventsFromEarlierInTheDay(int currentTime)
  {
    System.out.print("GetEventsFromEarlierInTheDay " + currentTime);
    boolean printFlag = false;
    ArrayList<String> printer = new ArrayList<String>();
    /*
     * This is yet another copy-paste method. Exactly like
     * ...RestOfTheDay, except searching backwards instead of
     * forwards. Also had to add the elements to an ArrayList
     * to print backwards.
     */
    Node p = eventMap.getHead(); // Start the search at the head,
                                 // which should be at the top

    while(p.getBelow() != null)  // If you can go down...
    {
      p = p.getBelow(); // ...proceed to do so
      while(currentTime / 100 >= p.getNext().getElement().getKey() / 100 &&
            p.getElement().getValue() != "oo")
      {
        p = p.getNext();
      }
    }

    // Go to the bottom after finding the approximate location
    // of the currentTime
    while(p.getBelow() != null)
      p = p.getBelow();

    // Fowards check
    while(p.getNext().getElement().getKey() <= currentTime)
      p = p.getNext();
    while(p.getElement().getKey() / 100 == currentTime / 100)
    {
      // Check for the key without the hours, instead of
      // just comparing keys.
      if (p.getElement().getKey() / 100 == currentTime / 100)
      {
        printer.add(" " + p.getElement().getKey() + ":" +
                           p.getElement().getValue());
        printFlag = true;
      }
      p = p.getPrev();
    }

    if(!printFlag)
    {
      System.out.println(" none");
    }
    else
    {
      for(int i = printer.size() - 1; i >= 0; i--)
        System.out.print(printer.get(i));
      System.out.println();
    }
  }

  public static void PrintSkipList()
  {
    System.out.println("PrintSkipList");
    System.out.println(eventMap.toString());
  }

  public static void main(String[] args) throws IOException
  {
    Scanner scan = new Scanner(new File(args[0]));

    String input, event, command;
    String[] split;
    int time1, time2;
    while(scan.hasNext())
    {
      input = scan.nextLine();
      split = input.split(" ");
      command = split[0];

      switch(command)
      {
        case "AddEvent":
          time1 = Integer.parseInt(split[1]);
          event = split[2];
          AddEvent(time1, event);
          break;
        case "CancelEvent":
          time1 = Integer.parseInt(split[1]);
          CancelEvent(time1);
          break;
        case "GetEvent":
          time1 = Integer.parseInt(split[1]);
          GetEvent(time1);
          break;
        case "GetEventsBetweenTimes":
          time1 = Integer.parseInt(split[1]);
          time2 = Integer.parseInt(split[2]);
          GetEventsBetweenTimes(time1, time2);
          break;
        case "GetEventsForOneDay":
          time1 = Integer.parseInt(split[1]);
          GetEventsForOneDay(time1);
          break;
        case "GetEventsForTheRestOfTheDay":
          time1 = Integer.parseInt(split[1]);
          GetEventsForTheRestOfTheDay(time1);
          break;
        case "GetEventsFromEarlierInTheDay":
          time1 = Integer.parseInt(split[1]);
          GetEventsFromEarlierInTheDay(time1);
          break;
        case "PrintSkipList":
          PrintSkipList();
          break;
        default:
          System.out.println("Command not recognized.\n");
      }
    }
  }
}
