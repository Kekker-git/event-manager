/*
 * Copyright 2014, Michael T. Goodrich, Roberto Tamassia, Michael H. Goldwasser
 *
 * Developed for use with the book:
 *
 *    Data Structures and Algorithms in Java, Sixth Edition
 *    Michael T. Goodrich, Roberto Tamassia, and Michael H. Goldwasser
 *    John Wiley & Sons, 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//package net.datastructures;

/**
 * A skip list implementation based on a heavily modified
 * doubly linked list from the credited authors.
 *
 * @author Michael T. Goodrich
 * @author Roberto Tamassia
 * @author Michael H. Goldwasser
 */
public class SkipListMap {
  // instance variables of the DoublyLinkedList
  private Node head;                   // head sentinel
  private Node tail;                   // tail sentinel

  private int n;                   // number of elements in the list

  private int height;              // height of the list
  FakeRandomHeight r;              // fake random generator

  private Event moo = new Event(Integer.MIN_VALUE, "-oo"); // negative infinity
  private Event  oo = new Event(Integer.MAX_VALUE, "oo");  // positive infinity

  /** Constructs a new empty list. */
  public SkipListMap() {
    head = new Node(moo, null, null, null, null);  // create head
    tail = new Node(oo,  head, null, null, null);   // tail is preceded by head
    head.setNext(tail); // head is followed by tail

    n = 0;      // Initialize number of elements to 0
    height = 0; // Do the same with height

    r = new FakeRandomHeight(); // Initialize the random number generator
  }

  // public accessor methods
  /**
   * Returns the number of elements in the linked list.
   * @return number of elements in the linked list
   */
  public int size()
  {
    return n;
  }

  /**
   * Tests whether the linked list is empty.
   * @return true if the linked list is empty, false otherwise
   */
  public boolean isEmpty()
  {
    return n == 0;
  }

  public Node getHead()
  {
    return head;
  }

  /**
   * Finds the node with the given key.
   * @param k
   * @return the node found
   */
  public Node get(int k)
  {
    Node p = head; // Start the search at the head,
                   // which should be at the top

    while(p.getBelow() != null) // If you can go down...
    {
      p = p.getBelow(); // ...proceed to do so
      while(k >= p.getNext().getElement().getKey() &&
            p.getElement().getKey() != oo.getKey())
      {
        p = p.getNext();
      }
    }
    return p;
  }

  /**
   * Finds the value at the given key.
   * @param k  the key to search for
   * @return   the key's corresponding value
   */
  public String getValue(int k)
  {
    Node p = get(k);

    return p.getElement().getValue();
  }

  /**
   * Adds a new entry to the skip list.
   * @param k  the key of the new entry
   * @param v  the value of the new entry
   * @return   the value of the new entry
   */
  public Event put(int k, String v)
  {
    Node p = get(k);
    if(p.getElement().getKey() == k)
      return p.getElement();
    int i = 0;

    if(k == p.getElement().getKey())
    {
      System.out.println("Duplicate key found. Breaking.");
      return null;
    }

    Event qevent = new Event(k, v);
    Node q = addBetween(qevent, p, p.getNext());

    /*System.out.println("Initial link " + q.getElement().getValue() +
                       " created between " + q.getPrev().getElement().getValue() +
                       ", " + q.getNext().getElement().getValue() +
                       " on level " + i);*/

    int random = r.get();

    while(i < random)
    {
      if(i >= height)
      {
        Node nHead = new Node(moo, null, null, null, null);
        Node nTail = new Node(oo,  null, null, null, null);

        nHead.setNext(nTail);
        nHead.setBelow(head);

        nTail.setPrev(nHead);
        nTail.setBelow(tail);

        head.setAbove(nHead);
        tail.setAbove(nTail);

        head = nHead;
        tail = nTail;

        height++;
      }

      while(p.getAbove() == null)
      {
        p = p.getPrev();
      }

      p = p.getAbove();

      Node e = addBetween(qevent, p, p.getNext());
      e.setBelow(q);
      q.setAbove(e);

      q = e;

      i++;
      /*System.out.println("Link " + e.getElement().getValue() +
                         " created on level " + i + " between " +
                         e.getPrev().getElement().getValue() + ", " +
                         e.getNext().getElement().getValue() + " for new height " +
                         random);*/
    }

    n++;
    return q.getElement();
  }

  public Event remove(int k)
  {
    Node node = get(k);
    Event ev = node.getElement();
    if(ev.getKey() != k)
      return ev;
    int count = 0;
    while(node != null)
    {
      Node temp = node.getAbove();
      removeLinks(node);
      node = temp;
      //System.out.println("Link removed on level " + count);
      count++;
    }
    return ev;
  }
  // private update methods
  /**
   * Adds an element to the linked list in between the given nodes.
   * The given predecessor and successor should be neighboring each
   * other prior to the call.
   *
   * @param predecessor   node just before the location where the new element is inserted
   * @param successor     node just after the location where the new element is inserted
   */
  private Node addBetween(Event e, Node predecessor, Node successor) {
    // create and link a new node
    Node newest = new Node(e, predecessor, successor, null, null);
    predecessor.setNext(newest);
    successor.setPrev(newest);
    n++;
    return newest;
  }

  /**
   * Removes the given node from the list and returns its element.
   * @param node    the node to be removed (must not be a sentinel)
   */
  private Event removeLinks(Node node) {
    Node predecessor = node.getPrev();
    Node successor = node.getNext();
    predecessor.setNext(successor);
    successor.setPrev(predecessor);
    n--;
    return node.getElement();
  }

  /**
   * Produces a string representation of the contents of the list.
   * This exists for debugging purposes only.
   */
  public String toString() {
    StringBuilder sb = new StringBuilder("(S" + (height + 1) + ") empty\n");
    Node tempH = head, walk;
    int counter = height;
    while(tempH != null)
    {
      walk = tempH.getNext();
      sb.append("(S" + counter + ") ");
      while (walk.getElement().getValue() != "oo") {
        sb.append(walk.getElement().getKey() + ":" +
                  walk.getElement().getValue());
        walk = walk.getNext();
        if (walk.getElement().getValue() != "oo")
          sb.append(" ");
      }
      if(tempH.getBelow() != null)
        sb.append("\n");
      // else
      //   sb.append("\n");
      tempH = tempH.getBelow();
      counter--;
    }

    return sb.toString();
  }
} //----------- end of SkipListMap class -----------
