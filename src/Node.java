public class Node {
  /** The element stored at this node */
  private Event event;               // reference to the event stored at this node

  /** A references to relative nodes in the list */
  private Node prev, next, above, below;

  /**
   * Creates a node with the given event and next node.
   *
   * @param e  the event to be stored
   * @param p  reference to a node that should precede the new node
   * @param n  reference to a node that should follow the new node
   * @param a  reference to a node that should be above the new node
   * @param b  reference to a node that should be below the new node
   */
  public Node(Event e, Node p, Node n, Node a, Node b) {
    event = e;
    prev = p;
    next = n;
    above = a;
    below = b;
  }

  // public accessor methods
  /**
   * Returns the event stored at the node.
   * @return the event stored at the node
   */
  public Event getElement() { return event; }

  /**
   * Returns the node that precedes this one (or null if no such node).
   * @return the preceding node
   */
  public Node getPrev() { return prev; }

  /**
   * Returns the node that follows this one (or null if no such node).
   * @return the following node
   */
  public Node getNext() { return next; }

  /**
   * Returns the node that is above this one (or null if no such node).
   * @return the above node
   */
  public Node getAbove() { return above; }

  /**
   * Returns the node that is below this one (or null if no such node).
   * @return the below node
   */
  public Node getBelow() { return below; }


  // Update methods
  /**
   * Sets the node's previous reference to point to Node n.
   * @param p    the node that should precede this one
   */
  public void setPrev(Node p) { prev = p; }

  /**
   * Sets the node's next reference to point to Node n.
   * @param n    the node that should follow this one
   */
  public void setNext(Node n) { next = n; }

  /**
   * Sets the node's above reference to point to Node a.
   * @param a    the node that should be above this one
   */
  public void setAbove(Node a) { above = a; }

  /**
   * Sets the node's below reference to point to Node b.
   * @param n    the node that should be below this one
   */
  public void setBelow(Node b) { below = b; }


}
