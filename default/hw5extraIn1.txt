AddEvent 050122 concert
AddEvent 040908 breakfast
AddEvent 052012 lunch
CancelEvent 040908
AddEvent 061019 dinner
AddEvent 041013 charity
AddEvent 122520 christmas
AddEvent 112415 thanksgiving
AddEvent 112506 black_friday
AddEvent 010100 new_year
AddEvent 031010 spring
GetEvent 122520
GetEventsBetweenTimes 031010 050122
GetEventsForOneDay 1125
GetEventsForTheRestOfTheDay 061012
GetEventsFromEarlierInTheDay 040911
AddWeeklyEvent 100204 3 doctorVisit
AddEvent 052123 party
AddEvent 041020 bowling
GetEventsForTomorrow 101510
GetEventsForTheComingWeekday 040612 Mon
GetEventsForTheComingWeekend 051508
PrintSkipList