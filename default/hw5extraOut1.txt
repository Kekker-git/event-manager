AddEvent 050122 concert
AddEvent 040908 breakfast
AddEvent 052012 lunch
CancelEvent 040908 breakfast
AddEvent 061019 dinner
AddEvent 041013 charity
AddEvent 122520 christmas
AddEvent 112415 thanksgiving
AddEvent 112506 black_friday
AddEvent 010100 new_year
AddEvent 031010 spring
GetEvent 122520 christmas
GetEventsBetweenTimes 031010 050122 031010:spring 041013:charity 050122:concert
GetEventsForOneDay 1125 112506:black_friday
GetEventsForTheRestOfTheDay 061012 061019:dinner
GetEventsFromEarlierInTheDay 040911 none
AddWeeklyEvent 100204 3 doctorVisit
AddEvent 052123 party
AddEvent 041020 bowling
GetEventsForTomorrow 101510 101604:doctorVisit
GetEventsForTheComingWeekday 040612 Mon 041013:charity 041020:bowling
GetEventsForTheComingWeekend 051508 052012:lunch 052123:party
PrintSkipList
(S4) empty
(S3) 112506:black_friday
(S2) 061019:dinner 100904:doctorVisit 112506:black_friday
(S1) 031010:spring 052123:party 061019:dinner 100904:doctorVisit 112506:black_friday 122520:christmas
(S0) 010100:new_year 031010:spring 041013:charity 041020:bowling 050122:concert 052012:lunch 052123:party 061019:dinner 100204:doctorVisit 100904:doctorVisit 101604:doctorVisit 112415:thanksgiving 112506:black_friday 122520:christmas